var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Data = new Schema({
	name: String,
	email: String,
	phone: String,
	message:String,
	dateSent: String,
});

module.exports = mongoose.model('data', Data);
