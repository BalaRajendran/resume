const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var Download = new Schema({
    user_token: String,
	name: String,
	provider:String,
	email: String,
	picture: String,
	dateSent: Date,
});

module.exports = mongoose.model('Download', Download);