var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Visitor = new Schema({
	count: { type: Number, default: 0 },
	dateSent: String,
});

module.exports = mongoose.model('visitor', Visitor);
