var passport = require('passport');
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var keys = require('../../config/keys');
var init = require('./init');
passport.use(
	new GoogleStrategy(
		{ 
			clientID: keys.googleClientID,
			clientSecret: keys.googleClientSecret,
			callbackURL: '/auth/google/callback',
		},
		(token, refreshToken, profile, done) => {
			return done(null, {
				profile: profile,
				token: token,
			});
		}
	)
);

init();

module.exports = passport;
