var passport = require('passport');
var LinkedInStrategy = require('passport-linkedin-oauth2').Strategy;
var keys = require('../../config/keys');
var init = require('./init');

passport.use(
	new LinkedInStrategy(
		{
			clientID: keys.linkedinClientID,
			clientSecret: keys.linkedinClientSecret,
			callbackURL: '/auth/linkedin/callback',
			scope: ['r_emailaddress'],
			state: true,
		},
		(token, refreshToken, profile, done) => {
			return done(null, {
				profile: profile,
				token: token,
				refreshToken:refreshToken
			});
		}
	)
);

init();

module.exports = passport;
