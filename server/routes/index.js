var express = require('express');
var router = express.Router();
const Data = require('../models/Data');
const Visitor = require('../models/Visitor');
const keys = require('../../config/keys');
const template = require('./template');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(keys.SENDGRID_API_KEY);

router.route('/backend/visitors').post(function(req, res, next) {
	const date = new Date().toLocaleString('en-US', {
		timeZone: 'Asia/Calcutta',
	});
	Visitor.findOneAndUpdate(
		{},
		{
			$inc: { count: 1, date: date },
		}
	).exec();
});
router.route('/backend/feedback').post(function(req, res, next) {
	const { name, email, phone, message } = req.body;
	const date = new Date().toLocaleString('en-US', {
		timeZone: 'Asia/Calcutta',
	});
	const data = new Data({
		name,
		email,
		phone,
		message,
		date,
	});
	const msg1 = {
		to: 'karurbalamathi@gmail.com',
		from: 'yourwebsite@gmail.com',
		fromname: name,
		reply_to: 'karurbalamathi@yahoo.com',
		subject: 'Query from your Website',
		html: template.htmltemplate,
	};
	const msg2 = {
		to: email,
		from: 'yourwebsite@gmail.com',
		fromname: name,
		reply_to: 'karurbalamathi@gmail.com',
		subject: 'From Balaji Rajendran',
		html: template.htmltemplate1,
	};
	try {
		data
			.save()
			.then(data => {
				sgMail
					.send(msg1)
					.then(() => {
						sgMail.send(msg2).then(() => {
							res.send('Thank you for your Response....');
						});
					})
					.catch(error => {
						res.send('Mail Not Send, Contact him Directly....');
					});
			})
			.catch(err => {
				res.send('Something Wrong! Try Again later');
			});
	} catch (err) {
		console.log('something wrong');
	}
});
module.exports = router;
