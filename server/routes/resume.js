var express = require('express');
var router = express.Router();
var session = require('express-session');
var app = express();
const keys = require('../../config/keys');
const template = require('./template');
const sgMail = require('@sendgrid/mail');
var Resume = require('../models/Download');
sgMail.setApiKey(keys.SENDGRID_API_KEY);
var passportLinkedIn = require('../auth/linkedin');
var passportGoogle = require('../auth/google');
router.get('/auth/linkedin', passportLinkedIn.authenticate('linkedin'));
router.get(
	'/auth/linkedin/callback',
	passportLinkedIn.authenticate('linkedin', { failureRedirect: '/' }),
	(req, res) => {
		const date = new Date().toLocaleString('en-US', {
			timeZone: 'Asia/Calcutta',
		});
		const nameFirst = req.user.profile._json.displayName;
		const picture = req.user.profile._json.pictureUrl;
		const email = req.user.profile.emails[0].value;
		const id = req.user.profile.id;
		const resume = new Resume({
			user_token: id,
			name: nameFirst,
			email: email,
			picture: picture,
			provider: 'LinkedIn',
			dateSent: date,
		});
		const msg1 = {
			to: 'karurbalamathi@gmail.com',
			from: 'yourwebsite@gmail.com',
			fromname: 'Balaji Rajendran',
			reply_to: 'karurbalamathi@yahoo.com',
			subject: 'Someone download your resume',
			html: template.htmltemplate4,
		};
		const msg2 = {
			to: email,
			from: 'yourwebsite@gmail.com',
			fromname: 'Balaji Rajendran',
			reply_to: 'karurbalamathi@gmail.com',
			subject: 'From Balaji Rajendran',
			html: template.htmltemplate2,
		};
		try {
			resume
				.save()
				.then(data => {
					sgMail
						.send(msg1)
						.then(() => {
							sgMail
								.send(msg2)
								.then(() => {
									req.session.token = id;
									res.redirect('/?success=Downloaded Successfully');
								})
								.catch(error => {
									res.redirect('/?success=Downloaded Successfully');
								});
						})
						.catch(error => {
							res.redirect('/?success=Downloaded Successfully');
						});
				})
				.catch(err => {
					console.log(err);
					res.send('Something Wrong! Try Again later');
				});
		} catch (err) {
			console.log('something wrong');
		}
	}
);
router.get(
	'/auth/google',
	passportGoogle.authenticate('google', {
		scope: [
			'https://www.googleapis.com/auth/userinfo.profile',
			'https://www.googleapis.com/auth/userinfo.email',
		],
	})
);

router.get(
	'/auth/google/callback',
	passportGoogle.authenticate('google', {
		failureRedirect: '/',
	}),
	(req, res) => {
		const date = new Date().toLocaleString('en-US', {
			timeZone: 'Asia/Calcutta',
		});
		const nameFirst = req.user.profile._json.displayName;
		const picture = req.user.profile._json.image.url;
		const email = req.user.profile.emails[0].value;
		const id = req.user.profile.id;
		const resume = new Resume({
			user_token: id,
			name: nameFirst,
			email: email,
			picture: picture,
			provider: 'Google',
			dateSent: date,
		});
		const msg1 = {
			to: 'karurbalamathi@gmail.com',
			from: 'yourwebsite@gmail.com',
			fromname: 'Balaji Rajendran',
			reply_to: 'karurbalamathi@yahoo.com',
			subject: 'Someone download your resume',
			html: template.htmltemplate4,
		};
		const msg2 = {
			to: email,
			from: 'yourwebsite@gmail.com',
			fromname: 'Balaji Rajendran',
			reply_to: 'karurbalamathi@gmail.com',
			subject: 'From Balaji Rajendran',
			html: template.htmltemplate2,
		};
		try {
			resume
				.save()
				.then(data => {
					sgMail
						.send(msg1)
						.then(() => {
							sgMail
								.send(msg2)
								.then(() => {
									req.session.token = id;
									res.redirect('/?success=Downloaded Successfully');
								})
								.catch(error => {
									res.redirect('/?success=Downloaded Successfully');
								});
						})
						.catch(error => {
							res.redirect('/?success=Downloaded Successfully');
						});
				})
				.catch(err => {
					console.log(err);
					res.send('Something Wrong! Try Again later');
				});
		} catch (err) {
			console.log('something wrong');
		}
	}
);
module.exports = router;
