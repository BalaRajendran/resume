var express = require('express');
var http = require('http');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var keys = require('./config/keys');
var mongoose = require('mongoose');
var passport = require('passport');
var routes = require('./server/routes/index.js');
var resume = require('./server/routes/resume.js');
var app = express();
var session = require('express-session');
app.use(session({ secret: "assa", saveUninitialized: true, resave: true }));
mongoose.Promise = require('bluebird');
mongoose.connect(
	keys.mongoURI,
	{ useNewUrlParser: true },
	function (err, db) {
		if (!err) {
			console.log('Database Connected!');
		} else {
			console.log('Database Not Connected!');
		}
	}
);
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(passport.initialize());
app.use(cookieParser());
app.use((req, res, next) => {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	res.header('Access-Control-Allow-Credentials', true);
	res.header('Access-Control-Allow-Methods', '*');
	return next();
});
app.use('/', routes);
app.use('/', resume);
var sslRedirect = require("heroku-ssl-redirect");
app.use(sslRedirect());
module.exports = app;
module.exports = function (environments, status) {
	environments = environments || ['production'];
	status = status || 302;
	return function (req, res, next) {
		if (environments.indexOf(process.env.NODE_ENV) >= 0) {
			if (req.headers['x-forwarded-proto'] != 'https') {
				res.redirect(status, 'https://' + req.hostname + req.originalUrl);
			}
			else {
				next();
			}
		}
		else {
			next();
		}
	};
};
app.get('/auth/current_user', (req, res, next) => {
	sess = req.session
	if (sess.token) {
		res.cookie('token', sess.token);
		res.json({
			status: sess.token,
		});
	} else {
		res.cookie('token', '');
		res.json({
			status: 'Access Denied',
		});
	}
});
app.get('/auth/logout', (req, res) => {
	res.cookie('token', req.session.token)
	if (req.session.token) {
		req.session.destroy(function (err) {
			if (err) {
				console.log(err);
			}
			else {
				var file = __dirname + '/Balaji_Rajendran _Resume.pdf';
				res.download(file)
			}
		});
	} else {
		res.send("ss")
	}
});
if (process.env.NODE_ENV === 'production') {
	app.use(express.static('client/build/'));
	const path = require('path');
	app.get('*', (req, res) => {
		res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
	});
}
const PORT = process.env.PORT || 5000;
var listener = http.createServer(app);
var listener = app.listen(PORT, function () {
	console.log('Server Started on port ' + listener.address().port);
});
