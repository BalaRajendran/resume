module.exports = {
	mongoURI: process.env.mongoURI,
	SENDGRID_API_KEY: process.env.SEND_GRID_KEY,
	googleClientID: process.env.googleClientID,
	googleClientSecret: process.env.googleClientSecret,
	linkedinClientID: process.env.linkedinClientID,
	linkedinClientSecret: process.env.linkedinClientSecret,
};
 