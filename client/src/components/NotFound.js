import React, { Component } from 'react';

class NotFound extends Component {
	render() {
		return (
			<div>
				<div className="error">
					<div className="container">
						<div className="row justify-content-center">
							<div className="col-xs-12 col-sm-12 col-md-12 col-lg-6">
								<div className="error-content">
									<div className="error-message">
										<h2>404</h2>
										<h3>
											<span>Ooooops!</span> Something Went Wrong...
										</h3>
									</div>
									<div className="description">
										<span>
											Or Goto <a href="/">Homepage</a>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
export default NotFound;
