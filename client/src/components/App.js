import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import Index from './Index';

class NotFound extends Component {
	render() {
		return (
			<center style={{ "marginTop": "300px" }}><h2>Not Found Go to Home page</h2></center>
		)
	}
}
class App extends Component {
	render() {
		return (
			<div className="wrapper-page">
				<div id="preloader">
					<div id="preloader-circle">
						<span />
						<span />
					</div>
				</div>
				<Switch>
					<Route path="/" component={Index} exact />
					<Route path="*" component={NotFound} exact />
				</Switch>
			</div>
		);
	}
}
export default App;
