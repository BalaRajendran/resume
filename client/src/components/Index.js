import React, { Component } from 'react';
import Header from './layouts/header/Header';
import Home from './layouts/pages/Home';
import About from './layouts/pages/About';
import Education from './layouts/pages/Education';
import Projects from './layouts/pages/Projects';
import Technical from './layouts/pages/Technical';
import Contact from './layouts/pages/Contact';
class Index extends Component {
	constructor(props) {
		super(props);
		this.state = {
			message: '',
		};
	}
	render() {
		return (
			<div>
				<Header />
				<div className="content-pages">
					<div className="sub-home-pages">
						<Home />
						<About />
						<Education />
						<Projects />
						<Technical />
						<Contact />
					</div>
				</div>
			</div>
		);
	}
}
export default Index;
