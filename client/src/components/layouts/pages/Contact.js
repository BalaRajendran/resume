import React, { Component } from 'react';
import axios from 'axios';
import Bottom from '../contact/bottom';
var $ = require('jquery');

class Index extends Component {
	constructor(props) {
		super(props);
		this.onChangeName = this.onChangeName.bind(this);
		this.onChangePhone = this.onChangePhone.bind(this);
		this.onChangeEmail = this.onChangeEmail.bind(this);
		this.onChangeFeedback = this.onChangeFeedback.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
		this.state = {
			nameerror: '',
			emailerror: '',
			phoneerror: '',
			feedbackerror: '',
			name: '',
			feedback: '',
			phone: '',
			email: '',
			message: '',
		};
	}
	onChangeName(e) {
		this.setState({
			nameerror: '',
			name: e.target.value,
		});
		if ((!/^[a-zA-Z. ]{4,30}$/.test(e.target.value)) && (e.target.value)) {
			this.setState({
				nameerror: 'Invalid Name',
				message: '',
			});
		}
	}
	onChangePhone(e) {
		this.setState({
			phoneerror: '',
			phone: e.target.value,
		});
		const re = /^([7-9]{1}[0-9]{9})$/;
		if ((re.test(e.target.value) === false) && (e.target.value)) {
			this.setState({
				phoneerror: 'Invalid Phone Number',
				message: '',
			});
		}
	}
	onChangeEmail(e) {
		this.setState({
			emailerror: '',
			email: e.target.value,
		});
		if ((!/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(e.target.value)) && (e.target.value)) {
			this.setState({
				emailerror: 'Invalid Email-ID',
				message: '',
			});
		}
	}
	onChangeFeedback(e) {
		this.setState({
			feedbackerror: '',
			feedback: e.target.value,
		});
		if ((e.target.value.length <= 10) && (e.target.value)) {
			this.setState({
				feedbackerror: 'Feedback / Query must be atlest 10 characters',
				message: '',
			});
		}
	}
	onSubmit(e) {
		e.preventDefault();
		let k = 0;
		this.setState({
			nameerror: '',
			emailerror: '',
			phoneerror: '',
			feedbackerror: '',
			message: 'Validating...',
		});
		if (!/^[a-zA-Z. ]{2,30}$/.test(this.state.name)) {
			this.setState({
				nameerror: 'Invalid Name',
				message: '',
			});
			$('.name').focus();
			k = 1;
		}
		if (!this.state.name) {
			this.setState({
				nameerror: 'Name Required',
				message: '',
			});
			$('.name').focus();
			k = 1;
		}
		if (!this.state.email) {
			this.setState({
				emailerror: 'Email-Id Required',
				message: '',
			});
			$('.email').focus();
			k = 1;
		} else if (!/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(this.state.email)) {
			this.setState({
				emailerror: 'Invalid Email-ID',
				message: '',
			});
			$('.roll0').focus();
			k = 1;
		}
		const re = /^([7-9]{1}[0-9]{9})$/;
		if (re.test(this.state.phone) === false) {
			this.setState({
				phoneerror: 'Invalid Phone Number',
				message: '',
			});
			$('.name').focus();
			k = 1;
		}
		if (!this.state.phone) {
			this.setState({
				phoneerror: 'Phone Number Required',
				message: '',
			});
			$('.phone').focus();
			k = 1;
		}
		if (this.state.feedback.length <= 10) {
			this.setState({
				feedbackerror: 'Feedback / Query must be atlest 10 characters',
				message: '',
			});
			$('.feedback').focus();
			k = 1;
		}
		if (!this.state.feedback) {
			this.setState({
				feedbackerror: 'Feedback Required',
				message: '',
			});
			$('.feedback').focus();
			k = 1;
		}
		if (k === 0) {
			this.setState({
				message: 'Loading...',
			});
			const data = {
				name: this.state.name,
				email: this.state.email,
				phone: this.state.phone,
				message: this.state.feedback,
			};
			var self = this;
			axios.post('/backend/feedback', data).then(function (res) {
				if (res.data === 'Thank you for your Response....' || res.data === 'Mail Not Send, Contact him Directly....') {
					self.setState({
						nameerror: '',
						emailerror: '',
						phoneerror: '',
						feedbackerror: '',
						message: res.data,
						name: '',
						feedback: '',
						phone: '',
						email: '',
					});
				} else {
					self.setState({
						message: 'Try Again Later...',
					});
				}
			});
		}
	}
	render() {
		return (
			<section id="contact" className="sub-page">
				<div className="sub-page-inner">
					<div className="section-title">
						<div className="main-title">
							<div className="title-main-page">
								<h4>Contact</h4>
								<p>NEED SOME HELP?</p>
							</div>
						</div>
					</div>

					<div className="row contact-form pb-30">
						<div className="col-sm-12 col-md-5 col-lg-5 left-background">
							<img src="images/mailbox.png" alt="Balaji Rajendran Porfolio" />
						</div>
						<div className="col-sm-12 col-md-7 col-lg-7">
							<div className="form-contact-me">
								{this.state.nameerror && (
									<div id="show_contact_msg">
										<div className="err">
											<i className="fa fa-frown-o" aria-hidden="true" />
											<strong>{this.state.nameerror}</strong>
										</div>
									</div>
								)}
								{this.state.emailerror && (
									<div id="show_contact_msg">
										<div className="err">
											<i className="fa fa-frown-o" aria-hidden="true" />
											<strong>{this.state.emailerror}</strong>
										</div>
									</div>
								)}
								{this.state.phoneerror && (
									<div id="show_contact_msg">
										<div className="err">
											<i className="fa fa-frown-o" aria-hidden="true" />
											<strong>{this.state.phoneerror}</strong>
										</div>
									</div>
								)}
								{this.state.feedbackerror && (
									<div id="show_contact_msg">
										<div className="err">
											<i className="fa fa-frown-o" aria-hidden="true" />
											<strong>{this.state.feedbackerror}</strong>
										</div>
									</div>
								)}
								{this.state.message && (
									<div id="show_contact_msg">
										<div className="gen">
											<i className="fa fa-smile-o" aria-hidden="true" />
											<strong>{this.state.message}</strong>
										</div>
									</div>
								)}
								<form
									onSubmit={this.onSubmit}
									id="contact-form"
									action="php/contact.php"
								>
									<input
										name="name"
										id="name"
										type="text"
										placeholder="Name:"
										autoComplete="off"
										value={this.state.name}
										onChange={this.onChangeName}
									/>
									<input
										name="email"
										id="email"
										type="text"
										placeholder="Email:"
										value={this.state.email}
										onChange={this.onChangeEmail}
										autoComplete="off"
									/>
									<input
										name="phone"
										id="phone"
										type="text"
										placeholder="Phone:"
										value={this.state.phone}
										onChange={this.onChangePhone}
										autoComplete="off"
									/>
									<textarea
										name="comment"
										id="comment"
										placeholder="Message:"
										value={this.state.feedback}
										onChange={this.onChangeFeedback}
										rows="6"
									/>
									<input
										className="bt-submit"
										type="submit"
										value="Send Message"
									/>
								</form>
							</div>
						</div>
					</div>

					<Bottom />
				</div>
			</section>
		);
	}
}
export default Index;
