import React, { Component } from 'react';
import { projectlinks as links } from '../../../data.json';

class Index extends Component {
	render() {
		return (
			<section id="portfolio" className="sub-page">
				<div className="sub-page-inner">
					<div className="section-title">
						<div className="main-title">
							<div className="title-main-page">
								<h4>Projects</h4>
								<p />
							</div>
						</div>
					</div>

					<div className="section-content">
						<div className="filter-tabs">
							{links.count.map((count, i) => (
								<button
									className="fil-cat"
									data-rel={links.secondtype[i]}
									key={i}
								>
									<span className="counter-block-value" data-count={count}>
										0
									</span>{' '}
									{links.firsttype[i]}
								</button>
							))}
						</div>
						<div
							className="portfolio-grid portfolio-trigger"
							id="portfolio-page"
						>
							<div className="label-portfolio">
								<span
									className="project-count counter-block-value"
									data-count="20"
								>
									0
								</span>
								<span className="rotated-sub">projects</span>
							</div>
							<div className="row">
								{links.img.map((img, i) => (
									<div
										className={`col-lg-6 col-md-6 col-sm-12 col-xs-12 portfolio-item  branding all ${
											links.type[i]
											}`}
										key={i}
									>
										<div className="portfolio-img">
											<img
												src={`images/projects/${img}`}
												className="img-responsive"
												alt="Project Images"
											/>
										</div>
										<div className="portfolio-data">
											<h4>
												<a href="#!">{links.app[i]}</a>
											</h4>
											<p className="meta">{links.lang[i]}</p>
											<div className="portfolio-attr">
												<a target="_blank" href={links.a[i]} rel="noopener noreferrer">
													<i className="lnr lnr-link" />
												</a>
												<a
													href={`images/projects/${img}`}
													data-rel="lightcase:gal"
													title={links.app[i]}
												>
													<i className="lnr lnr-move" />
												</a>
											</div>
										</div>
									</div>
								))}
							</div>
						</div>
					</div>
				</div>
			</section>
		);
	}
}
export default Index;
