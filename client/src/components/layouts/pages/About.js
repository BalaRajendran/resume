import React, { Component } from "react";
import Learning from "../About/Learning";
import Facts from "../About/Facts";
import Topcut from "../About/Topcut";
import Client from "../About/Clients";

class Index extends Component {
  render() {
    return (
      <section id="about-me" className="sub-page">
        <div className="sub-page-inner">
          <div className="section-title">
            <div className="main-title">
              <div className="title-main-page">
                <h4>About me</h4>
                <p />
              </div>
            </div>
          </div>
          <div className="section-content">
            <div className="row">
              <div className="section-head col-sm-8" />
              <div
                className="section-head col-sm-4"
              >
                <a className="bt-submit" href="images/Balaji Rajendran _Resume_withoutpic.zip" target="_blank">
                  <i className="fas fa-cloud-download-alt" /> Download Resume
                </a>
              </div>
            </div>
            <Topcut />
            <Learning />
            <Facts />
            <Client />
          </div>
        </div>
      </section>
    );
  }
}
export default Index;
