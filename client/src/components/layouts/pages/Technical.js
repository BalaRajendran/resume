import React, { Component } from 'react';
import Remain from '../Technical/Index';

class Index extends Component {
	render() {
		return (
			<section id="blog" className="sub-page">
				<div className="sub-page-inner">
					<div className="section-title">
						<div className="main-title">
							<div className="title-main-page">
								<h4>Technical Skill</h4>
								<p />
							</div>
						</div>
					</div>
					<Remain />
				</div>
			</section>
		);
	}
}
export default Index;
