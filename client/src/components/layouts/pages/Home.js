import React, { Component } from "react";
class Home extends Component {
  render() {
    return (
      <section id="home" className="sub-page start-page">
        <div
          className="sub-page-inner"
          style={{ background: "url('images/home-bg.jpeg')" }}
        >
          <div className="mask" />
          <div className="row">
            <div className="col-sm-12 col-md-12 col-lg-12">
              <div className="title-block">
                <h2>Hello I'm Balaji Rajendran</h2>
                <div className="type-wrap">
                  <div className="typed-strings">
                    <span>Full Stack Developer</span>
                    <span>Machine Learning Engineer</span>
                    <span>Application Developer</span>
                  </div>
                  <span className="typed" style={{ whiteSpace: "pre" }} />
                </div>
                <div className="home-buttons">
                  <a href="#contact" className="bt-submit">
                    <i className="lnr lnr-envelope" /> Contact Me
                  </a>
                  <a href="#contact" className="bt-submit">
                    <i className="lnr lnr-briefcase" /> Hire Me
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
export default Home;
