import React, { Component } from 'react';
import { educationlinks as links } from '../../../data.json';

class Index extends Component {
	render() {
		return (
			<section id="resume" className="sub-page">
				<div className="sub-page-inner">
					<div className="section-title">
						<div className="main-title">
							<div className="title-main-page">
								<h4>Education</h4>
								<p />
							</div>
						</div>
					</div>

					<div className="section-content">
						<div className="row">
							<div className="col-xs-12 col-sm-12 col-md-12">
								<div className="pt-30">
									<div className="section-head">
										<h4>
											<span>My Education</span>
											Background History
										</h4>
									</div>
									<div className="main-timeline">
										{links.img.map((img, i) => (
											<div className="timeline" key={i}>
												<div className="timeline-icon">
													<img src={img} alt="" />
												</div>
												<div className="timeline-content">
													<span className="date" >{links.year[i]}</span>
													<h5 className="title" >{links.title[i]}</h5>
													<p className="description" >{links.place[i]}</p>
												</div>
											</div>
										))}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		);
	}
}
export default Index;
