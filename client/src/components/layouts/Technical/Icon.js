import React, { Component } from 'react';
import { skilllinks as links } from '../../../data.json';

class Index extends Component {
	class(a) {
		if (a[0] !== '-') {
			return <i style={{ fontSize: '73px' }} className={a} />;
		} else {
			const c = a.split(' ');
			return <img alt="Balaji Rajendran Porfolio" src={`images/${c[1]}`} style={{ width: '70px' }} />;
		}
	}
	render() {
		return (
			<div className="row blog-grid-flex">
				{links.classnames.map((classnames, i) => (
					<div
						title={links.title[i]}
						className="col-md-2 col-sm-4 blog-item"
						key={i}
					>
						<div className="blog-article" style={{ textAlign: 'center' }}>
							<a href="#!">{this.class(classnames)}</a>
						</div>
					</div>
				))}
			</div>
		);
	}
}
export default Index;
