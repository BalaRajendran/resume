import React, { Component } from 'react';

class Index extends Component {
    render() {
        return (
            <div className="col-md-5">
                <div className="special-block-bg">
                    <div className="section-head">
                        <h4>
                            <span>My Professional</span>
                            Language Skills
									</h4>
                    </div>
                    <div className="skills-items">
                        <div className="language-skill row">
                            <h4 className="col-md-6 text-left">
                                English <span>Fluent</span>
                            </h4>
                            <ul className="col-md-6 text-right rating">
                                <li>
                                    <i className="fa fa-star" />
                                </li>
                                <li>
                                    <i className="fa fa-star" />
                                </li>
                                <li>
                                    <i className="fa fa-star" />
                                </li>
                                <li>
                                    <i className="fa fa-star" />
                                </li>
                            </ul>
                        </div>
                        <div className="language-skill row">
                            <h4 className="col-md-6 text-left">
                                Tamil <span>Fluent</span>
                            </h4>
                            <ul className="col-md-6 text-right rating">
                                <li>
                                    <i className="fa fa-star" />
                                </li>
                                <li>
                                    <i className="fa fa-star" />
                                </li>
                                <li>
                                    <i className="fa fa-star" />
                                </li>
                                <li>
                                    <i className="fa fa-star" />
                                </li>
                                <li>
                                    <i className="fa fa-star" />
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Index;
