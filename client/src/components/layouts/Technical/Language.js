import React, { Component } from 'react';
import { skilllinks as links } from '../../../data.json';

class Index extends Component {
	skills(a, b, i) {
		const c = a;
		const b1 = b;
		if (
			c === 'High Level Language' ||
			c === 'Full Stack Frameworks' ||
			c === 'Full Stack' ||
			c === 'DataBase'
		) {
			return (
				<h4 key={i}>
					<span>{c}</span>
				</h4>
			);
		} else {
			return (
				<div className="skill-item" key={i}>
					<h4>{c}</h4>
					<div className="progress">
						<div
							className="progress-bar wow fadeInLeft"
							data-progress={`${b1}`}
							style={{ width: `${b1}` }}
							data-wow-duration="2.5s"
							data-wow-delay="1.2s"
						/>
					</div>
					<span>{b1}</span>
				</div>
			);
		}
	}
	render() {
		return (
			<div className="col-md-7">
				<div className="special-block-bg">
					<div className="section-head">
						<h4>
							<span>My Professional </span>
							Work Skills
						</h4>
					</div>
					<div className="skills-items skills-progressbar">
						{links.language.map((language, i) =>
							this.skills(language, links.percent[i], i)
						)}
					</div>
				</div>
			</div>
		);
	}
}
export default Index;
