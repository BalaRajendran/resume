import React, { Component } from 'react';
import Speak from './Speak';
import Language from './Language'
import Icon from './Icon';

class Index extends Component {
	render() {
		return (
			<div className="section-content">
				<Icon />
				<div className="pb-30 pt-30">
					<div className="row list-skills">
						<Language />
						<Speak />
					</div>
				</div>
			</div>
		);
	}
}
export default Index;
