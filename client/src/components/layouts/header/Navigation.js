import React, { Component } from 'react';
class Header extends Component {
	render() {
		return (
			<div>
				<li>
					<a className="active" href="#home">
						<i className="fas fa-home" /> Home
					</a>
				</li>
				<li>
					<a href="#about-me">
						<i className="fas fa-user-tie" /> About Me
					</a>
				</li>
				<li>
					<a href="#resume">
						<i className="fas fa-award" /> Education
					</a>
				</li>
				<li>
					<a href="#blog">
						<i className="fas fa-business-time" />
						Technical Skills
					</a>
				</li>
				<li>
					<a href="#portfolio">
						<i className="fas fa-book-reader" /> Projects
					</a>
				</li>
				<li>
					<a href="#contact">
						<i className="fas fa-paper-plane" /> Contact
					</a>
				</li>
			</div>
		);
	}
}
export default Header;
