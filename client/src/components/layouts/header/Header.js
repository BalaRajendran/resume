import React, { Component } from "react";
import Navigation from "./Navigation";

class Header extends Component {
  render() {
    return (
      <div>
        <header className="header">
          <div className="header-content">
            <div className="profile-picture-block">
              <div className="my-photo">
                <img
                  src="images/avatar.jpeg"
                  className="img-fluid"
                  alt="Balaji Rajendran Porfolio"
                />
              </div>
            </div>
            <div className="site-title-block">
              <div className="site-title">Balaji Rajendran</div>
              <div className="type-wrap">
                <div className="typed-strings">
                  <span>Full Stack Developer</span>
                  {/* <span>Machine Learning Engineer</span> */}
                  <span>Application Developer</span>
                </div>
                <span className="typed" style={{ whiteSpace: "pre" }} />
              </div>
            </div>
            <div className="site-nav">
              <ul className="header-main-menu" id="header-main-menu">
                <Navigation />
              </ul>
            </div>
          </div>
        </header>
        <div className="responsive-header">
          <div className="responsive-header-name">
            <img className="responsive-logo" src="images/avatar.jpeg" alt="Balaji Rajendran Porfolio" />
            Balaji Rajendran
          </div>
          <span className="responsive-icon">
            <i className="lnr lnr-menu" />
          </span>
        </div>
      </div>
    );
  }
}
export default Header;
