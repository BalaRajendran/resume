import React, { Component } from 'react';
import { contactlinks as links } from '../../../data.json';

class Index extends Component {
	render() {
		return (
			<div className="pt-50 pb-30">
				<div className="section-head">
					<h4>
						<span>Contact Information</span>
						Find me here
					</h4>
				</div>

				<div className="sidebar-textbox row pb-50">
					<div className="contact-info d-flex col-md-4">
						<div className="w-25">
							<div className="contact-icon">
								<i className="fas fa-phone" />
							</div>
						</div>
						<div className="contact-text w-75">
							<h2>Phone</h2>
							<p>+91 95667 11894</p>
							<p>+91 90805 48527 </p>
						</div>
					</div>
					<div className="contact-info d-flex col-md-4">
						<div className="w-25">
							<div className="contact-icon">
								<i className="far fa-envelope-open" />
							</div>
						</div>
						<div className="contact-text w-75">
							<h2>Email</h2>
							<p>karurbalamathi@gmail.com</p>
							<p>karurbalamathi@yahoo.com</p>
						</div>
					</div>
					<div className="contact-info d-flex col-md-4">
						<div className="w-25">
							<div className="contact-icon">
								<i className="fas fa-map-marker-alt" />
							</div>
						</div>
						<div className="contact-text w-75">
							<h2>Address</h2>
							<p>Karur, Tamil Nadu</p>
						</div>
					</div>
				</div>

				<div className="contact-map pt-50">
					<div id="google-map" />
				</div>

				<div className="pt-50">
					<div className="social-media-block">
						<h4>Follow Me: </h4>
						<ul className="social-media-links">
							{links.classic.map((classic, i) => (
								<li key={i}>
									<a
										target="_blank"
										rel="noopener noreferrer"
										href={links.a[i]}
									>
										<i className={classic} />
									</a>
								</li>
							))}
						</ul>
					</div>
				</div>
			</div>
		);
	}
}
export default Index;
