import React, { Component } from 'react';
import { feedbacklinks as links } from '../../../data.json';

class Index extends Component {
	render() {
		return (
			<div className="special-block-bg">
				<div className="section-head">
					<h4>
						<span>What My Freedom</span>
						Client Say
					</h4>
				</div>
				<div className="testimonials owl-carousel">
					{links.feedback.map((feedback, i) => (
						<div className="testimonial-item" key={i}>
							<div className="testimonial-content">
								<div className="testimonial-review">
									<p>{feedback}</p>
								</div>
							</div>
							<div className="testimonial-footer">
								<div className="testimonial-avatar">
									<img src={links.img[i]} alt="Gary Johnson" />
								</div>
								<div className="testimonial-owner-content">
									<p className="testimonial-owner">{links.name[i]}</p>
									<p className="testimonial-position">{links.des[i]}</p>
								</div>
							</div>
						</div>
					))}
				</div>
			</div>
		);
	}
}
export default Index;
