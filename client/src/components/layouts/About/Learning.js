import React, { Component } from 'react';
import { learninglinks as links } from '../../../data.json';

class Index extends Component {
	render() {
		return (
			<div className="special-block-bg">
				<div className="section-head">
					<h4>
						<span>What Actually I Do</span>
						My Experience
					</h4>
				</div>
				<div className="row">
					{links.name.map((name, i) => (
						<div className="col-xs-12 col-sm-12 col-md-6 col-lg-6" key={i}>
							<div className="services-list">
								<div className="service-block">
									<div className="service-icon">
										<i className="lnr lnr-code" />
									</div>
									<div className="service-text">
										<h4>{name}</h4>
										<p>{links.descrip[i]}</p>
									</div>
								</div>
							</div>
						</div>
					))}
				</div>
			</div>
		);
	}
}
export default Index;
