import React, { Component } from 'react';
import { factlinks as links } from '../../../data.json';

class Index extends Component {
	render() {
		return (
			<div className="row pb-30 pt-30">
				<div className="section-head col-sm-12">
					<h4>
						<span>Fun</span>
						Facts
					</h4>
				</div>
				{links.name.map((name, i) => (
					<div className="col-xs-12 col-sm-6 col-md-4 col-lg-4" key={i}>
						<div className="pb-30">
							<div className="counter-block">
								<i className={links.class[i]} />
								<h4>{name}</h4>
								<span className="counter-block-value" data-count={links.count[i]}>
									0
								</span>
							</div>
						</div>
					</div>
				))}
			</div>
		);
	}
}
export default Index;
