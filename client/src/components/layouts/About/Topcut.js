import React, { Component } from 'react';
import Experience from './Experience';

class Index extends Component {
	render() {
		return (
			<div className="row pb-30">
				<div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<h3>Balaji Rajendran</h3>
					<span className="about-location">
						<i className="lnr lnr-map-marker" /> Tamil Nadu, India
					</span>
					<p className="about-content">
						Always waiting to pursue a dynamic and a challenging career that
						offers a creative and challenging work environment, together with
						job satisfaction and steady Professional growth
					</p>
					<Experience />
				</div>

				<div className="col-xs-6 col-sm-12 col-md-6 col-lg-6">
					<div className="box-img">
						<img src="images/about.png" className="img-fluid" alt="Balaji Rajendran Porfolio" />
					</div>
				</div>
			</div>
		);
	}
}
export default Index;
