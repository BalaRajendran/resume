import React, { Component } from 'react';
import { clientslinks as links } from '../../../data.json';

class Index extends Component {
	render() {
		return (
			<div className="row pt-50">
				<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div className="section-head">
						<h4>
							<span>My</span>
							Testimonal
						</h4>
					</div>
					<div className="clients owl-carousel">
						{links.link.map((link, i) => (
							<div className="client-block" key={i}>
								<a
									href={`images/clients/${link}.jpeg`}
									data-rel="lightcase:gal"
									title={"Certificates"}
								>
									<img src={`images/clients/${link}.jpeg`} alt="Certificates" />
								</a>
							</div>
						))}
					</div>
				</div>
			</div>
		);
	}
}
export default Index;
