import React, { Component } from 'react';
import { experiencelinks as links } from '../../../data.json';

class Index extends Component {
	render() {
		return (
			<ul className="bout-list-summry row">
				{links.first.map((first, i) => (
					<li className="col-xs-12 col-sm-12 col-md-4 col-lg-4" key={i}>
						<div className="icon-info">
							<i className="lnr lnr-briefcase" />
						</div>
						<div className="details-info">
							<h6>{first}</h6>
							<p>{links.second[i]}</p>
						</div>
					</li>
				))}
			</ul>
		);
	}
}
export default Index;
